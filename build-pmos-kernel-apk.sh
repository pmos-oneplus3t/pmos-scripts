pmbootstrap build linux-postmarketos-qcom-msm8996 --envkernel
# stop tinydm
echo 1234 | ssh user@172.16.42.1 sudo -S /etc/init.d/tinydm stop
# clean up apk cache
echo 1234 | ssh user@172.16.42.1 sudo -S rm /var/cache/apk/*.apk
# update kernel modules
pmbootstrap sideload linux-postmarketos-qcom-msm8996
echo 1234 | ssh user@172.16.42.1 sudo -S reboot

echo "boot to fastboot mode, press any key to boot new kernel"
read

#pmbootstrap flasher boot
pmbootstrap flasher flash_kernel
#echo check flashed boot.img date
#ls -la ~/pmos/pmbootstrap/chroot_rootfs_oneplus-oneplus3t/boot/boot.img

#fastboot reboot
